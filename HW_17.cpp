﻿#include <iostream>
#include <cmath>

class Integer
{
public:
    int GetABC()
    {
        std::cout << a << ' ' << b << ' ' << c << '\n';
        return 0;
    }
    void SetABC(int newA, int newB, int newC)
    {
        a = newA;
        b = newB;
        c = newC;
    }
    int GetSum()
    {
        return a + b + c;
    }

private:
    int a;
    int b;
    int c;
};

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void ShowVector()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
        std::cout << '\n' << sqrt(x * x + y * y + z * z);
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Integer temp;
    temp.SetABC(2, 4, 8);
    temp.GetABC();
    std::cout << temp.GetSum();


    Vector V(10, 5, 2);
    V.ShowVector();
}